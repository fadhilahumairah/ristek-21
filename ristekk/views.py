from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request,"home.html")

def liked(request):
    return render(request,"liked.html")

def bookmark(request):
    return render(request,"bookmark.html")
