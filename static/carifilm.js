$(document).ready(function () {
  cariFilm();
  halamanLiked();
  halamanBookmark();

  const like = localStorage.getItem("liked");
  let likeArr = "";

  if (like == "" || like == null) {
    likeArr = [];
  } else {
    likeArr = like.split(";");
  }

  const bookmark = localStorage.getItem("bookmark");
  let bookmarkArr = "";
  if (bookmark == "" || bookmark == null) {
    bookmarkArr = [];
  } else {
    bookmarkArr = bookmark.split(";");
  }

  $("#inputSearch").on("keyup", function (e) {
    if ($(this).val().length != 0) {
      $("#iconSearch").hide();
      $.ajax({
        url: "https://www.omdbapi.com/?apikey=8639b59e&s=" + $(this).val(),
        success: function (data) {
          $("#kotakFilm").html("");
          var result = "";
          data.Search.forEach((film) => {
            if (film.Poster != "N/A") {
              result = `<div class='ini'><img style='width:100%; height: 100%; border-radius:7px' src='
                            ${
                              film.Poster
                            }'> <div class='poster'></div> <div class='movieDetail'> <span style='font-weight:bold'>${
                film.Title
              }</span><br><span>${
                film.Year
              }</span><br><div style='display:flex; flex-direction:row; justify-content:center'>
                            <i id='liked' class='${
                              likeArr.includes(film.imdbID)
                                ? "bi-heart-fill"
                                : "bi-heart"
                            } p-3 jarak' onclick='like(this,"${
                film.imdbID
              }")'></i> <i id='bookmark' class='${
                bookmarkArr.includes(film.imdbID)
                  ? "bi-bookmark-fill"
                  : "bi-bookmark"
              } p-3 jarak' onclick='bookmark(this,"${
                film.imdbID
              }")'></i></div></div></div>`;
              $("#kotakFilm").append(result);
            }
          });
        },
      });
    } else {
      $("#iconSearch").show();
      cariFilm();
    }
  });
});

function cariFilm() {
  const like = localStorage.getItem("liked");
  let likeArr = "";

  if (like == "" || like == null) {
    likeArr = [];
  } else {
    likeArr = like.split(";");
  }

  const bookmark = localStorage.getItem("bookmark");
  let bookmarkArr = "";
  if (bookmark == "" || bookmark == null) {
    bookmarkArr = [];
  } else {
    bookmarkArr = bookmark.split(";");
  }
  $.ajax({
    url: "https://www.omdbapi.com/?apikey=8639b59e&s=marvel",

    success: function (data) {
      $("#kotakFilm").html("");

      var result = "";
      data.Search.forEach((film) => {
        if (film.Poster != "N/A") {
          result = `<div class='ini'><img style='width:100%; height: 100%; border-radius:7px' src='
                    ${
                      film.Poster
                    }'> <div class='poster'></div> <div class='movieDetail'> <span style='font-weight:bold'>${
            film.Title
          }</span><br><span>${
            film.Year
          }</span><br><div style='display:flex; flex-direction:row; justify-content:center'>
                    <i id='liked' class='${
                      likeArr.includes(film.imdbID)
                        ? "bi-heart-fill"
                        : "bi-heart"
                    } p-3 jarak' onclick='like(this,"${
            film.imdbID
          }")'></i> <i id='bookmark' class='${
            bookmarkArr.includes(film.imdbID)
              ? "bi-bookmark-fill"
              : "bi-bookmark"
          } p-3 jarak' onclick='bookmark(this,"${
            film.imdbID
          }")'></i></div></div></div>`;
          $("#kotakFilm").append(result);
        }
      });
    },
  });
}

function halamanLiked() {
  const liked = localStorage.getItem("liked");
  let likedArr = null;
  if (liked == "" || liked == null) {
    likedArr = null;
  } else {
    likedArr = liked.split(";");
  }

  const bookmark = localStorage.getItem("bookmark");
  let bookmarkArr = "";
  if (bookmark == "" || bookmark == null) {
    bookmarkArr = [];
  } else {
    bookmarkArr = bookmark.split(";");
  }
  $("#kotakLiked").html("");
  if (likedArr != null) {
    var result = "";
    for (var i = 0; i < likedArr.length; i++) {
      $.ajax({
        url: "https://www.omdbapi.com/?apikey=8639b59e&i=" + likedArr[i],
        success: function (film) {
          result = `<div class='ini'><img style='width:100%; height: 100%; border-radius:7px' src='
                    ${
                      film.Poster
                    }'> <div class='poster'></div> <div class='movieDetail'> <span style='font-weight:bold'>${
            film.Title
          }</span><br><span>${
            film.Year
          }</span><br><div style='display:flex; flex-direction:row; justify-content:center'>
                    <i id='liked' class='bi-heart-fill p-3 jarak' onclick='like(this,"${
                      film.imdbID
                    }")'></i> <i id='bookmark' class='${
            bookmarkArr.includes(film.imdbID)
              ? "bi-bookmark-fill"
              : "bi-bookmark"
          } p-3 jarak' onclick='bookmark(this,"${
            film.imdbID
          }")'></i></div></div></div>`;
          $("#kotakLiked").append(result);
        },
      });
    }
  }
}

function halamanBookmark() {
  const bookmark = localStorage.getItem("bookmark");
  let bookmarkArr = null;
  if (bookmark == "" || bookmark == null) {
    bookmarkArr = null;
  } else {
    bookmarkArr = bookmark.split(";");
  }

  const like = localStorage.getItem("liked");
  let likeArr = "";

  if (like == "" || like == null) {
    likeArr = [];
  } else {
    likeArr = like.split(";");
  }
  $("#kotakBookmark").html("");
  if (bookmarkArr != null) {
    var result = "";
    for (var i = 0; i < bookmarkArr.length; i++) {
      $.ajax({
        url: "https://www.omdbapi.com/?apikey=8639b59e&i=" + bookmarkArr[i],
        success: function (film) {
          result = `<div class='ini'><img style='width:100%; height: 100%; border-radius:7px' src='
                    ${
                      film.Poster
                    }'> <div class='poster'></div> <div class='movieDetail'> <span style='font-weight:bold'>${
            film.Title
          }</span><br><span>${
            film.Year
          }</span><br><div style='display:flex; flex-direction:row; justify-content:center'>
                    <i id='liked' class='${
                      likeArr.includes(film.imdbID)
                        ? "bi-heart-fill"
                        : "bi-heart"
                    } p-3 jarak' onclick='like(this,"${
            film.imdbID
          }")'></i> <i id='bookmark' class='bi-bookmark-fill p-3 jarak' onclick='bookmark(this,"${
            film.imdbID
          }")'></i></div></div></div>`;
          $("#kotakBookmark").append(result);
        },
      });
    }
  }
}

function like(e, q) {
  if ($(e).hasClass("bi-heart")) {
    $(e).removeClass("bi-heart").addClass("bi-heart-fill");

    const simpan = localStorage.getItem("liked");
    if (simpan == null || simpan == "") {
      localStorage.setItem("liked", `${q}`);
    } else {
      let simpanArr = simpan.split(";");
      let result = "";
      for (let i = 0; i < simpanArr.length; i++) {
        if (simpanArr[i] != `${q}`) {
          result = "${q}";
        } else {
          result = null;
          break;
        }
      }

      if (result != null) {
        localStorage.setItem("liked", `${simpan};${q}`);
      }
    }
  } else {
    $(e).removeClass("bi-heart-fill").addClass("bi-heart");

    const simpan = localStorage.getItem("liked");

    let simpanArr = simpan.split(";");
    for (let i = 0; i < simpanArr.length; i++) {
      if (simpanArr[i] == `${q}`) {
        if (i > -1) {
          simpanArr.splice(i, 1);
          break;
        }
      }
    }
    let hasil = simpanArr.join(";");
    localStorage.removeItem("liked");
    localStorage.setItem("liked", `${hasil}`);
  }

  halamanLiked();
}

function bookmark(e, q) {
  if ($(e).hasClass("bi-bookmark")) {
    $(e).removeClass("bi-bookmark").addClass(" bi-bookmark-fill");

    const simpan = localStorage.getItem("bookmark");
    if (simpan == null || simpan == "") {
      localStorage.setItem("bookmark", `${q}`);
    } else {
      let simpanArr = simpan.split(";");
      let result = "";

      for (let i = 0; i < simpanArr.length; i++) {
        if (simpanArr[i] != `${q}`) {
          result = "${q}";
        } else {
          result = null;
          break;
        }
      }

      if (result != null) {
        localStorage.setItem("bookmark", `${simpan};${q}`);
      }
    }
  } else {
    $(e).removeClass("bi-bookmark-fill").addClass("bi-bookmark");

    const simpan = localStorage.getItem("bookmark");

    let simpanArr = simpan.split(";");
    for (let i = 0; i < simpanArr.length; i++) {
      if (simpanArr[i] == `${q}`) {
        if (i > -1) {
          simpanArr.splice(i, 1);
          break;
        }
      }
    }
    let hasil = simpanArr.join(";");
    localStorage.removeItem("bookmark");
    localStorage.setItem("bookmark", `${hasil}`);
  }

  halamanBookmark();
}
